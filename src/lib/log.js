"use strict"; 
import { green, cyan, bold, yellow, red, magenta } from 'chalk';
import moment from 'moment';
import { dirname } from 'path';
import pathExists from 'path-exists';
import mkdirp from 'mkdirp';
import { EOL } from 'os';
import { getOrCreateConfig } from './dirs';
const config = getOrCreateConfig();
import { existsSync, readFileSync, appendFile, appendFileSync } from 'fs';
import { mapSeries } from 'async';
import { neutralizeColor, reenableColor, stripColor } from '../lib/util';
import { EventEmitter } from 'events';
import expandTilde from 'expand-tilde';
import logRotate from 'log-rotate';

const { log: outLn, error: errLn } = console;

const SINK_INTERVAL = 50;

const LOG_MAX_LINES = 5000;

const out = process.stdout.write.bind(process.stdout),
    err = process.stderr.write.bind(process.stderr);

Object.setPrototypeOf(log, EventEmitter.prototype);
EventEmitter.call(log);

let sink = [];
let logPath;
if (config.log) {
  logPath = config.log.path;
  if (logPath) logPath = expandTilde(logPath);
} else config.log = {};

let sync;

if (logPath) {
    try {
        mkdirp(dirname(logPath));
        log.isLogging = true;
        if (existsSync(logPath)) log._lineCount = countLines(readFileSync(logPath, 'utf8'));
        else log._lineCount = 0;
    } catch (e) {
        log.isLogging = false;
        log.processError(e);
        log.infoLn(`creating log at ${green(config.logPath)} failed${EOL}${bold(e.stack)}`);
        log.infoLn('logging disabled');
    }
} else log.isLogging = false;

let running = log.isLogging;

const newLnRe = /(?:\r\n|\n|\r)/g;

function countLines(str) {
  return str.split(newLnRe).length;
}

log._countLines = countLines;

const appendLog = appendFile.bind(null, logPath);

const rotate = logRotate.bind(null, logPath, { compress: config.log.gzip, count: config.log.rotate || 5 });

function addToSink(data) {
    let transformed = sinkTransform.call(sink, data);
    log.emit('add', transformed);
    if (sync) {
      try {
        appendLog(transformed);
        log._lineCount += countLines(transformed);
        if (log._lineCount > LOG_MAX_LINES) {
          rotate((err) => {
            if (err) log.processError(err);
            log._lineCount = 0;
          });
        }
      } catch(e) { log.processError(e); }
    }
    else sink.push(transformed);
    log.isFlushing = true;
}



log.processError = (err) => {
  log.errorLn(err.message);
  log.debugLnDecor(err.stack);
};

function scheduleFlush() {
    let i = 0;
    mapSeries(sink, (v, next) => {
      appendLog(v, (err) => {
        if (!err) {
          log._lineCount += countLines(v);
          if (log._lineCount > LOG_MAX_LINES) {
            rotate((err) => {
              next(err);
            });
          } else next();
        } else next(err);
      });
    }, (err, result) => {
        if (err) log.processError(err);
        log.isFlushing = false;
        sink.length = 0;
        log.emit('flush');
        if (running) setTimeout(function() {
            scheduleFlush();
        }, SINK_INTERVAL);
    });
}

if (running) scheduleFlush();

log.interruptSink = () => {
  running = false;
  sync = true;
};

log.enableSink = () => {
    if (running) return;
    running = true;
    sync = false;
    scheduleFlush();
};

const decorRe = /^\[([^\s\]]+)\]/;

function sinkTransform(data) {
  data = stripColor(data);
  data = data.replace(decorRe, `[$1 ${moment().format('MM-DD hh:mm:ss')}]`);
  return data;
}

function log(data) {
    return log.writeLn(data);
}
log.writeLn = function(data) {
    outLn(data);
    addToSink(data + EOL);
};


log.setDebugging = function(bool) {
    log._debugging = bool;
};

log.debug = function(data) {
    if (log._debugging) {
        log(data);
        addToSink(data);
    }
    return data;
};

log.debugNotify = function(data) {
    let message;
    if (log._debugging) {
        log((message = `${bold(yellow('[debug]'))} ${data}`));
        addToSink(message + EOL);
    }
    return data;
};

log.debugLnDecor = log.debugNotify;

log.debug = function(data) {
    if (log._debugging) {
        log.write(data);
        addToSink(data);
    }
};

log.errorLnDecor = function(data) {
    let message;
    errLn((message = `${red(bold('[error]'))} ${data}`));
    addToSink(message + EOL);
    return data;
};

log.errorLn = function(data) {
    errLn(data);
    addToSink(data + EOL);
    return data;
};

log.fatalLnDecor = function(err) {
    let message;
    errLn((message = `${red(bold('[fatal]'))} ${err.message}`));
    addToSink(message + EOL);
    if (log._debugging) {
        errLn(err.stack);
        addToSink(err.stack + EOL);
    }
    return err;
};

log.fatalLn = function(err) {
    errLn(err.message);
    addToSink(err.message + EOL);
    if (log._debugging) {
        errLn(err.stack);
        addToSink(err.stack + EOL);
    }
    return err;
};

log.eventLnDecor = function(event, data) {
    let message;
    outLn((message = `${magenta(bold('[event:' + String(event) + ']'))} ${data}`));
    addToSink(message + EOL);
    return data;
};

log.shutdown = function(code) {
    if (typeof code === 'undefined') code = 0;
    log.eventLnDecor('shutdown', moment().format('MM-DD-YYYY hh:mm:ss'));
};

log.info = function (data) {
  let message;
  out((message = `${cyan(bold('[info] '))}${data}`));
  addToSink(message);
};

log.infoLn = function (data) {
  let message;
  outLn((message = `${cyan(bold('[info] '))}${data}`));
  addToSink(message + EOL);
};

log.write = function(data) {
    out(data);
    addToSink(data);
    return data;
};

module.exports = log;
