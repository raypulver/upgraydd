"use strict";

import Web3 from 'web3';
import abi from '../../fixtures/DCASSET';
import { range, zipObject } from 'lodash';
import { mapSeries } from 'async';
import { writeFileSync } from 'fs';
import { relative, isAbsolute, join } from 'path';
import log from './log';
import BN from 'big-number';

const web3 = new Web3(new Web3.providers.HttpProvider('http://localhost:8545'));

const DCASSET = web3.eth.contract(abi);

function DCClient(config = {}) {
  if (!(this instanceof DCClient)) return new DCClient(config);
  this.config = config;
  this.target = DCASSET.at(config.addr);
  this.from = web3.eth.accounts[0];
}

function useContract(method, contract, func, args) {
  if (!args) args = [];
  return new Promise((resolve, reject) => {
    let contractFunc = contract[func];
    args.push(function (err, result) {
      if (err) return reject(err);
      resolve(result);
    });
    contractFunc[method].apply(contractFunc, args);
  });
}
const call = curry(useContract, 'call');
const transact = curry(useContract, 'sendTransaction');

DCClient.prototype = {
  siphon,
  upload,
  go
};

function curry(fn) {
  var args = [].slice.call(arguments, 1);
  return function () {
    return fn.apply(this, args.concat([].slice.call(arguments)));
  };
}

const curryNull = function (fn) {
  return function (result) {
    return fn(null, result);
  };
};

function go() {
  return this[this.config.action]();
}

function siphon() {
  log.infoLn(`beginning siphon from ${this.config.addr}`);
  let accts = [];
  let state = {};
  const callOnTarget = curry(call, this.target);
  const self = this;
  return callOnTarget('extractAccountLength').then((result) => {
    result = +new BN(result).toString(10);
    return new Promise((res, rej) => {
      mapSeries(range(result), (v, next) => {
        return callOnTarget('extractAccount', [v]).then(curryNull(next)).catch(next);
      }, (err, results) => {
        if (err) return rej(err);
        res(results);
      });
    });
  }).then((_accts) => {
    accts = _accts;
    return new Promise((res, rej) => {
      mapSeries(accts, (v, next) => {
        return callOnTarget('extractAccountBalance', [v]).then(curryNull(next)).catch(next);
      }, (err, results) => {
        if (err) return rej(err);
        res(results.map(function (v) { return +new BN(v).toString(); }));
      });
    });
  }).then((balances) => {
    state.balanceOf = zipObject(accts, balances);
    log.infoLn('balanceOf siphoned');
    return new Promise((res, rej) => {
      mapSeries(accts, (v, next) => {
        return callOnTarget('extractAccountAllowanceRecordLength', [v]).then((length) => {
          length = +new BN(length).toString(10);
          mapSeries(range(length), (idx, next) => {
            return callOnTarget('extractAccountAllowanceAddr', [v, idx]).then(curryNull(next)).catch(next);
          }, (err, addresses) => {
            mapSeries(addresses, (addr, next) => {
              return callOnTarget('extractAccountAllowance', [v, addr]).then(curryNull(next).catch(next));
            }, (err, results) => {
              next(err, zipObject(addresses, results.map(function (v) {
                return +new BN(v).toString(10);
              })));
            });
          });
        }).catch(next);
      }, (err, results) =>  {
        if (err) return rej(err);
        res(results);
      });
    });
  }).then((allowances) => {
    state.allowance = zipObject(accts, allowances);
    log.infoLn('allowance siphoned');
    return new Promise((res, rej) => {
      mapSeries(accts, (v, next) => {
        callOnTarget('extractAccountFrozenStatus', [v]).then(curryNull(next)).catch(next);
      }, (err, results) => {
        if (err) return rej(err);
        res(results);
      });
    });
  }).then((frozenStatuses) => {
    state.frozenAccount = zipObject(accts, frozenStatuses);
    log.infoLn('frozenAccount siphoned');
    writeFileSync(self.config.output, JSON.stringify(state, null, 1));
    log.infoLn(`state saved to ${relative(process.cwd(), self.config.output)}`);
  }).catch((err) => {
    console.log(err.stack);
  });
}

function upload() {
  let self = this;
  let state = require(isAbsolute(self.config.input) ? self.config.input : join(process.cwd(), self.config.input));
  const callOnTarget = transact.bind(null, this.target);
  log.infoLn(`uploading state from ${self.config.input} to ${self.config.addr}`);
  return new Promise((res, rej) => {
    log.infoLn('uploading balanceOf');
    mapSeries(Object.keys(state.balanceOf), (v, next) => {
      return callOnTarget('setAccountBalance', [v, state.balanceOf[v], { from: this.from }]).then(curryNull(next));
    }, (err, results) => {
      if (err) rej(err);
      res();
    });
  }).then(() => {
    log.infoLn('uploading allowance');
    return new Promise((res, rej) => {
      mapSeries(Object.keys(state.allowance), (from, next) => {
        mapSeries(Object.keys(state.allowance[from]), (to, next) => {
          return callOnTarget('setAccountAllowance', [from, to, state.allowance[from][to], { from: this.from }]).then(curryNull(next));
        }, (err, results) => {
          next(err, results);
        });
      }, (err, results) => {
        if (err) return rej(err);
        res();
      });
    });
  }).then(() => {
    log.infoLn('uploading frozenAccount');
    return new Promise((res, rej) => {
      mapSeries(Object.keys(state.frozenAccount), (v, next) => {
        return callOnTarget('setAccountFrozenStatus', [v, true, { from: this.from }]).then(curryNull(next));
      }, (err, results) => {
        if (err) return rej(err);
        res();
      });
    });
  }).then(() => {
    log.infoLn('done!');
  });
}


module.exports = DCClient;
