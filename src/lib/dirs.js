"use strict";

import { join } from 'path';
import { homedir } from 'os';
import { difference } from 'lodash';
import pathExists from 'path-exists';
import mkdirp from 'mkdirp';
import { writeFileSync } from 'fs';
import packageJson from '../../package';

const priv = join(homedir(), `.${packageJson.name}`);
function getOrCreateConfig() {
  let defaultCfg = require(join(__dirname, '..', '..', 'fixtures', 'config.json'));
  if (!pathExists.sync(priv)) mkdirp.sync(priv);
  if (!pathExists.sync(join(priv, 'config.json'))) writeFileSync(join(priv, 'config.json'), JSON.stringify(defaultCfg, null, 1));
  return require(join(priv, 'config.json'));
}
module.exports = {
  priv: join(homedir(), `.${packageJson.name}`),
  ethereumKeys: join(homedir(), '.ethereum', 'keystore'),
  getOrCreateConfig
}

