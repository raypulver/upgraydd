#!/usr/bin/env node

"use strict";

import Getopt from 'node-getopt';
import log from '../lib/log';
import { join, basename, dirname } from 'path';
import { green, cyan, magenta, yellow, bold, gray } from 'chalk';
import { neutralizeColor } from '../lib/util';
import { EOL, homedir } from 'os';
import pathExists from 'path-exists';
import { writeFileSync, readFileSync } from 'fs';
import mkdirp from 'mkdirp';
import sprintf from 'sprintf';
import { priv, getOrCreateConfig } from '../lib/dirs';
import { mapSeries } from 'async';
import moment from 'moment';
import { range } from 'lodash';
import packageJson from '../../package';
import expandTilde from 'expand-tilde';

import Client from '../lib/client';

const { exit } = process;

const getopt = new Getopt([
  ['h', 'help', 'display this help'],
  ['n', 'no-color', 'disable colored output'],
  ['c', 'config=CONFIGPATH', 'custom config file path'],
  ['v', 'version', 'display version number'],
  ['a', 'addr=ADDRESS', 'address of target contract'],
  ['s', 'state=STATE', 'path to JSON state file to upload or siphon to']
]);

process.title = basename(process.argv[1]);

let opts = getopt.parseSystem();

if (opts.options['no-color']) neutralizeColor();

const formatCommand = sprintf.bind(null, "  %-25s %-20s");

const formatCommandColoredLn = function () {
  return sprintf("%s%s", gray(bold(formatCommand.apply(null, arguments))), EOL);
}

getopt.setHelp(`${yellow(bold('Usage:'))} ${yellow(bold(process.title))} ${yellow(bold('[-vhn] [-c CONFIG] [-s STATEFILE] [-a ADDRESS] COMMAND'))}${EOL}${yellow('Commands are:')}${EOL}${formatCommandColoredLn('siphon', 'extract an existing contract state')}${formatCommandColoredLn('upload', 'upload state to new contract')}${yellow('Options are:')}${EOL}${gray(bold('[[OPTIONS]]'))}`);

if (opts.options.help) {
  getopt.showHelp();
  exit(0);
}

if (opts.options.version) {
  log(packageJson.version);
  exit(0);
}

log.setDebugging(opts.options.debug)

let config = getOrCreateConfig();

config.state = expandTilde(config.state);

if (!opts.argv.length) {
  log.errorLnDecor('Must supply a command');
  getopt.showHelp();
  exit(1);
}

if (opts.options.addr) config.addr = opts.options.addr;
if (opts.options.state) config.state = opts.options.state;
config.input = config.state, config.output = config.state;

config.action = opts.argv[0];

Client(config).go()
  .then(() => { exit(0); },
      (err) => {
        log.errorLnDecor(err.stack);
        exit(1);
      });
