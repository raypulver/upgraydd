"use strict";

const gulp = require('gulp'),
  path = require('path'),
  fs = require('fs'),
  _ = require('lodash'),
  runSequence = require('run-sequence'),
  mocha = require('gulp-mocha'),
  babel = require('gulp-babel');

let packageJson = require('./package');

gulp.task('default', ['build']);

const reserved = ['test', 'start', 'default'];

const srcPath = 'src/**/*.js';

function buildTestTask(testFramework) {
  return `node ${path.join('node_modules', testFramework, 'bin', testFramework)}`;
}

gulp.task('build:tasks', function () {
  packageJson.scripts = {};
  _.forOwn(gulp.tasks, function (value, key, obj) {
    if (~reserved.indexOf(key)) return;
    packageJson.scripts[key] = `node ${path.join('node_modules', 'gulp', 'bin', 'gulp')} ${key}`;
  });
  packageJson.scripts.test = buildTestTask('mocha');
  fs.writeFileSync('./package.json', JSON.stringify(packageJson, null, 1));
});

gulp.task('build', function (cb) {
  runSequence(['build:private', 'test'], cb);
});

gulp.task('watch', ['default'], function () {
  return gulp.watch([ srcPath ], ['default']);
});

gulp.task('test', function () {
  return gulp.src('test/test.js', { read: false })
    .pipe(mocha({ reporter: 'nyan' }));
});

gulp.task('build:private', function () {
  gulp.src('src/**/*.js')
    .pipe(babel({ presets: ['es2015'] }))
    .pipe(gulp.dest('dist'));
});
