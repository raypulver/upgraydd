# upgraydd

NOTICE: This repo is no longer maintained.. The functionality of `upgraydd` has been completely moved into `dcasset`. Have a nice day.

## Overview

Transfer state of a DCASSET to a new instance using indices built into the contract.

## Install

```shell
npm install -g
```

This will install `upgraydd` to your system.

## Usage

To use `upgraydd`, launch your RPC on port 8545 and make sure you are the owner of the existing DCASSET contract you are targetting. You can pass `siphon` or `upload` as an argument to `upgraydd` to save the state of a contract into a JSON file or upload the contract state from the JSON file. Pass `-s somefile.json` to specify the filepath to the state file, and pass `-a 0xTargetContractAddress` to specify the address of the DCASSET instance you are siphoning from or uploading to.

## Example

```shell
upgraydd -a 0x7438976c7ce4aee0eac5055d0b1db898d1b87b02 -s state.json siphon
// extract the state from an existing contract and store in state.json
upgraydd  -a 0x7438976c7ce4aee0eac5055d0b1db898d1b87b03 -s state.json upload
// upload the state from state.json to a new contract
```

## Note

This application only works on DCASSET instances with `extractFoo` and `setFoo` functions defined for all state-level mappings, which must be indexed with dynamic arrays. If new mappings are added to the contract they must be indexed and code must be added for the contract owner to be able to update or read any value in the new mappings, both in the contract and in `upgraydd`.

## Author

Raymond Pulver IV
