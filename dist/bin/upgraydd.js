#!/usr/bin/env node


"use strict";

var _nodeGetopt = require('node-getopt');

var _nodeGetopt2 = _interopRequireDefault(_nodeGetopt);

var _log = require('../lib/log');

var _log2 = _interopRequireDefault(_log);

var _path = require('path');

var _chalk = require('chalk');

var _util = require('../lib/util');

var _os = require('os');

var _pathExists = require('path-exists');

var _pathExists2 = _interopRequireDefault(_pathExists);

var _fs = require('fs');

var _mkdirp = require('mkdirp');

var _mkdirp2 = _interopRequireDefault(_mkdirp);

var _sprintf = require('sprintf');

var _sprintf2 = _interopRequireDefault(_sprintf);

var _dirs = require('../lib/dirs');

var _async = require('async');

var _moment = require('moment');

var _moment2 = _interopRequireDefault(_moment);

var _lodash = require('lodash');

var _package = require('../../package');

var _package2 = _interopRequireDefault(_package);

var _expandTilde = require('expand-tilde');

var _expandTilde2 = _interopRequireDefault(_expandTilde);

var _client = require('../lib/client');

var _client2 = _interopRequireDefault(_client);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _process = process;
var exit = _process.exit;


var getopt = new _nodeGetopt2.default([['h', 'help', 'display this help'], ['n', 'no-color', 'disable colored output'], ['c', 'config=CONFIGPATH', 'custom config file path'], ['v', 'version', 'display version number'], ['a', 'addr=ADDRESS', 'address of target contract'], ['s', 'state=STATE', 'path to JSON state file to upload or siphon to']]);

process.title = (0, _path.basename)(process.argv[1]);

var opts = getopt.parseSystem();

if (opts.options['no-color']) (0, _util.neutralizeColor)();

var formatCommand = _sprintf2.default.bind(null, "  %-25s %-20s");

var formatCommandColoredLn = function formatCommandColoredLn() {
  return (0, _sprintf2.default)("%s%s", (0, _chalk.gray)((0, _chalk.bold)(formatCommand.apply(null, arguments))), _os.EOL);
};

getopt.setHelp((0, _chalk.yellow)((0, _chalk.bold)('Usage:')) + ' ' + (0, _chalk.yellow)((0, _chalk.bold)(process.title)) + ' ' + (0, _chalk.yellow)((0, _chalk.bold)('[-vhn] [-c CONFIG] [-s STATEFILE] [-a ADDRESS] COMMAND')) + _os.EOL + (0, _chalk.yellow)('Commands are:') + _os.EOL + formatCommandColoredLn('siphon', 'extract an existing contract state') + formatCommandColoredLn('upload', 'upload state to new contract') + (0, _chalk.yellow)('Options are:') + _os.EOL + (0, _chalk.gray)((0, _chalk.bold)('[[OPTIONS]]')));

if (opts.options.help) {
  getopt.showHelp();
  exit(0);
}

if (opts.options.version) {
  (0, _log2.default)(_package2.default.version);
  exit(0);
}

_log2.default.setDebugging(opts.options.debug);

var config = (0, _dirs.getOrCreateConfig)();

config.state = (0, _expandTilde2.default)(config.state);

if (!opts.argv.length) {
  _log2.default.errorLnDecor('Must supply a command');
  getopt.showHelp();
  exit(1);
}

if (opts.options.addr) config.addr = opts.options.addr;
if (opts.options.state) config.state = opts.options.state;
config.input = config.state, config.output = config.state;

config.action = opts.argv[0];

(0, _client2.default)(config).go().then(function () {
  exit(0);
}, function (err) {
  _log2.default.errorLnDecor(err.stack);
  exit(1);
});