"use strict";

var _web = require('web3');

var _web2 = _interopRequireDefault(_web);

var _DCASSET = require('../../fixtures/DCASSET');

var _DCASSET2 = _interopRequireDefault(_DCASSET);

var _lodash = require('lodash');

var _async = require('async');

var _fs = require('fs');

var _path = require('path');

var _log = require('./log');

var _log2 = _interopRequireDefault(_log);

var _bigNumber = require('big-number');

var _bigNumber2 = _interopRequireDefault(_bigNumber);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var web3 = new _web2.default(new _web2.default.providers.HttpProvider('http://localhost:8545'));

var DCASSET = web3.eth.contract(_DCASSET2.default);

function DCClient() {
  var config = arguments.length <= 0 || arguments[0] === undefined ? {} : arguments[0];

  if (!(this instanceof DCClient)) return new DCClient(config);
  this.config = config;
  this.target = DCASSET.at(config.addr);
  this.from = web3.eth.accounts[0];
}

function useContract(method, contract, func, args) {
  if (!args) args = [];
  return new Promise(function (resolve, reject) {
    var contractFunc = contract[func];
    args.push(function (err, result) {
      if (err) return reject(err);
      resolve(result);
    });
    contractFunc[method].apply(contractFunc, args);
  });
}
var call = curry(useContract, 'call');
var transact = curry(useContract, 'sendTransaction');

DCClient.prototype = {
  siphon: siphon,
  upload: upload,
  go: go
};

function curry(fn) {
  var args = [].slice.call(arguments, 1);
  return function () {
    return fn.apply(this, args.concat([].slice.call(arguments)));
  };
}

var curryNull = function curryNull(fn) {
  return function (result) {
    return fn(null, result);
  };
};

function go() {
  return this[this.config.action]();
}

function siphon() {
  _log2.default.infoLn('beginning siphon from ' + this.config.addr);
  var accts = [];
  var state = {};
  var callOnTarget = curry(call, this.target);
  var self = this;
  return callOnTarget('extractAccountLength').then(function (result) {
    result = +new _bigNumber2.default(result).toString(10);
    return new Promise(function (res, rej) {
      (0, _async.mapSeries)((0, _lodash.range)(result), function (v, next) {
        return callOnTarget('extractAccount', [v]).then(curryNull(next)).catch(next);
      }, function (err, results) {
        if (err) return rej(err);
        res(results);
      });
    });
  }).then(function (_accts) {
    accts = _accts;
    return new Promise(function (res, rej) {
      (0, _async.mapSeries)(accts, function (v, next) {
        return callOnTarget('extractAccountBalance', [v]).then(curryNull(next)).catch(next);
      }, function (err, results) {
        if (err) return rej(err);
        res(results.map(function (v) {
          return +new _bigNumber2.default(v).toString();
        }));
      });
    });
  }).then(function (balances) {
    state.balanceOf = (0, _lodash.zipObject)(accts, balances);
    _log2.default.infoLn('balanceOf siphoned');
    return new Promise(function (res, rej) {
      (0, _async.mapSeries)(accts, function (v, next) {
        return callOnTarget('extractAccountAllowanceRecordLength', [v]).then(function (length) {
          length = +new _bigNumber2.default(length).toString(10);
          (0, _async.mapSeries)((0, _lodash.range)(length), function (idx, next) {
            return callOnTarget('extractAccountAllowanceAddr', [v, idx]).then(curryNull(next)).catch(next);
          }, function (err, addresses) {
            (0, _async.mapSeries)(addresses, function (addr, next) {
              return callOnTarget('extractAccountAllowance', [v, addr]).then(curryNull(next).catch(next));
            }, function (err, results) {
              next(err, (0, _lodash.zipObject)(addresses, results.map(function (v) {
                return +new _bigNumber2.default(v).toString(10);
              })));
            });
          });
        }).catch(next);
      }, function (err, results) {
        if (err) return rej(err);
        res(results);
      });
    });
  }).then(function (allowances) {
    state.allowance = (0, _lodash.zipObject)(accts, allowances);
    _log2.default.infoLn('allowance siphoned');
    return new Promise(function (res, rej) {
      (0, _async.mapSeries)(accts, function (v, next) {
        callOnTarget('extractAccountFrozenStatus', [v]).then(curryNull(next)).catch(next);
      }, function (err, results) {
        if (err) return rej(err);
        res(results);
      });
    });
  }).then(function (frozenStatuses) {
    state.frozenAccount = (0, _lodash.zipObject)(accts, frozenStatuses);
    _log2.default.infoLn('frozenAccount siphoned');
    (0, _fs.writeFileSync)(self.config.output, JSON.stringify(state, null, 1));
    _log2.default.infoLn('state saved to ' + (0, _path.relative)(process.cwd(), self.config.output));
  }).catch(function (err) {
    console.log(err.stack);
  });
}

function upload() {
  var _this = this;

  var self = this;
  var state = require((0, _path.isAbsolute)(self.config.input) ? self.config.input : (0, _path.join)(process.cwd(), self.config.input));
  var callOnTarget = transact.bind(null, this.target);
  _log2.default.infoLn('uploading state from ' + self.config.input + ' to ' + self.config.addr);
  return new Promise(function (res, rej) {
    _log2.default.infoLn('uploading balanceOf');
    (0, _async.mapSeries)(Object.keys(state.balanceOf), function (v, next) {
      return callOnTarget('setAccountBalance', [v, state.balanceOf[v], { from: _this.from }]).then(curryNull(next));
    }, function (err, results) {
      if (err) rej(err);
      res();
    });
  }).then(function () {
    _log2.default.infoLn('uploading allowance');
    return new Promise(function (res, rej) {
      (0, _async.mapSeries)(Object.keys(state.allowance), function (from, next) {
        (0, _async.mapSeries)(Object.keys(state.allowance[from]), function (to, next) {
          return callOnTarget('setAccountAllowance', [from, to, state.allowance[from][to], { from: _this.from }]).then(curryNull(next));
        }, function (err, results) {
          next(err, results);
        });
      }, function (err, results) {
        if (err) return rej(err);
        res();
      });
    });
  }).then(function () {
    _log2.default.infoLn('uploading frozenAccount');
    return new Promise(function (res, rej) {
      (0, _async.mapSeries)(Object.keys(state.frozenAccount), function (v, next) {
        return callOnTarget('setAccountFrozenStatus', [v, true, { from: _this.from }]).then(curryNull(next));
      }, function (err, results) {
        if (err) return rej(err);
        res();
      });
    });
  }).then(function () {
    _log2.default.infoLn('done!');
  });
}

module.exports = DCClient;