/**
 * @module lib/util
 * @description Contains all helper functions used by the application
 */

"use strict";

var _clone = require('clone');

var _clone2 = _interopRequireDefault(_clone);

var _chalk = require('chalk');

var _chalk2 = _interopRequireDefault(_chalk);

var _path = require('path');

var _path2 = _interopRequireDefault(_path);

var _pathExists = require('path-exists');

var _pathExists2 = _interopRequireDefault(_pathExists);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var nullifier = {
  open: '',
  close: ''
};

/**
 * @description
 * Explicitly disables the action of chalk, preventing color from being added
 * @returns {boolean} Returns false if chalk was already disabled
 */

module.exports.neutralizeColor = function neutralizeColor() {
  if (!neutralizeColor.cache) {
    neutralizeColor.cache = (0, _clone2.default)(_chalk2.default.styles);
    Object.keys(_chalk2.default.styles).forEach(function (v) {
      Object.assign(_chalk2.default.styles[v], nullifier);
    });
    return true;
  }
  return false;
};

/**
 * @description
 * Re-enable chalk if it was previously disabled
 * @returns {boolean} Returns false if chalk was already enabled
 */

module.exports.reenableColor = function reenableColor() {
  if (!neutralizeColor.cache) return false;
  _chalk2.default.styles = neutralizeColor.cache;
  delete neutralizeColor.cache;
  return true;
};

/**
 * @description
 * Joins the arguments into a path using require('path').join and feeds it to require('path-exists').sync
 * @returns {boolean} Whether or not the path exists on the file system
 */

module.exports.pathJoinExistsSync = function () {
  return _pathExists2.default.sync(_path2.default.join.apply(null, arguments));
};

/**
 * @description
 * Splices out an event handler from an EventEmitter instance that does not implement EventEmitter#off
 * @returns {void}
 */

module.exports.spliceHandler = function (sock, evt, fn) {
  if (Array.isArray(sock._events[evt])) {
    var idx = sock._events[evt].indexOf(fn);
    if (!~idx) return;
    sock._events[evt].splice(idx, 1);
    return;
  }
  if (typeof sock._events[evt] === 'function') delete sock._events[evt];
};

var colorRe = /\x1B\[([0-9]{1,2}(;[0-9]{1,2})?)?[m|K]/g;

/**
 * @description Removes color codes from string using regular expression
 * @param {string} str The string to remove color codes from
 * @returns {string} The string with color codes removed
 */

function stripColor(str) {
  return str.replace(colorRe, '');
}

module.exports.stripColor = stripColor;