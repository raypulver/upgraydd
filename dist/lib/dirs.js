"use strict";

var _path = require('path');

var _os = require('os');

var _lodash = require('lodash');

var _pathExists = require('path-exists');

var _pathExists2 = _interopRequireDefault(_pathExists);

var _mkdirp = require('mkdirp');

var _mkdirp2 = _interopRequireDefault(_mkdirp);

var _fs = require('fs');

var _package = require('../../package');

var _package2 = _interopRequireDefault(_package);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var priv = (0, _path.join)((0, _os.homedir)(), '.' + _package2.default.name);
function getOrCreateConfig() {
  var defaultCfg = require((0, _path.join)(__dirname, '..', '..', 'fixtures', 'config.json'));
  if (!_pathExists2.default.sync(priv)) _mkdirp2.default.sync(priv);
  if (!_pathExists2.default.sync((0, _path.join)(priv, 'config.json'))) (0, _fs.writeFileSync)((0, _path.join)(priv, 'config.json'), JSON.stringify(defaultCfg, null, 1));
  return require((0, _path.join)(priv, 'config.json'));
}
module.exports = {
  priv: (0, _path.join)((0, _os.homedir)(), '.' + _package2.default.name),
  ethereumKeys: (0, _path.join)((0, _os.homedir)(), '.ethereum', 'keystore'),
  getOrCreateConfig: getOrCreateConfig
};